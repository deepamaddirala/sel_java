package Package1;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class TC_002_PageModel {
	WebDriver driver;

	public  TC_002_PageModel(WebDriver driver) {
		this.driver = driver;
	}
	By signin = By.className("signin");
	By username = By.name("login");
	By password = By.name("passwd");
	By signinbutton = By.name("proceed");
	
	public WebElement signin_button() {
		return (driver.findElement(signin));
	}
	public WebElement EmailID() {
		return (driver.findElement(username));
	}

	public WebElement Password() {
		return (driver.findElement(password));
	}

	public WebElement Go() {
		return (driver.findElement(signinbutton));
	}

}
